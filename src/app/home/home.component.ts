import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { BaseComponent } from '@app/core/base/BaseComponent';
import { UsfServiceService } from '@app/core/usf/usf-service.service';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {
  numberCase: '';
  isLoading: boolean;
  constructor(
    public authenticationService: AuthenticationService,
    public usfServiceService: UsfServiceService,
    public router: Router,
    public fb: FormBuilder
  ) {
    super(authenticationService, usfServiceService, router, fb);
    this.usfServiceService.setValue('validateSSNData', null);
    this.usfServiceService.setValue('dataObjectAddress', null);
    this.usfServiceService.setValue('ssn', null);
    this.usfServiceService.setValue('requiredDocuments', null);
    this.usfServiceService.setValue('dataAgencyMoneySelection', null);
  }

  ngOnInit() {
    this.isLoading = true;
    this.numberCase = '';
  }

  goToUniversalService() {
    this.router.navigate(['/universal-service/personal-dates'], { replaceUrl: true }).then();
  }

  gotoUsfCase() {
    localStorage.setItem('numberCaseToSearch', this.numberCase);
    this.router.navigate(['/usf-case'], { replaceUrl: true }).then();
  }
}
